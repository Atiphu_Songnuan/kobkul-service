const express = require("express");
const cors = require("cors");
const mysql = require('mysql');

var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "kobkul"
});

con.connect(function (err) {
    if (err) throw err;
    let app = express()
    app.use(cors());

    let port = process.env.PORT || 1354

    app.listen(port, () => {
        console.log("listening on port " + port);
    });
    
    app.get("/api/students/", (req, res) => {
        // let service = req.params;
        let sql = "SELECT * FROM view_all_student"
        con.query(sql, function (err, result) {
            if (err) throw err;
            let student = {
                "data":[]
            };
            let studentClass = [];
            let classID = "";
            let studentRoom = [];
            let roomID = "";

            result.forEach(element => {
               if (classID != element.CLASS_ID) {
                   classID = element.CLASS_ID;
               } else{
                    if (roomID != element.ROOM_ID) {
                        roomID = element.ROOM_ID;
                    } else{
                        studentRoom.push(element);
                    }
               }
            });          
            res.status(200);
            res.send(JSON.stringify(studentRoom));
        });
       
    });
    app.get("/api/students/:class/:room", (req, res) => {
        let params = req.params;
        let class_id = params.class;
        let room_id = params.room;
        // res.send({
        //     "class": service.class,
        //     "room": service.room
        // });
        let sql = "SELECT * FROM view_all_student WHERE CLASS_ID = ? AND ROOM_ID = ?"
        let values = [];
        values = [class_id, room_id];
        con.query(sql, values, (err, result) => {
            if (err) throw err;
            res.send(result);
        });
        // con.query(sql, function (err, result) {
        //     if (err) throw err;
        //     let student = {
        //         "data":[]
        //     };
        //     let studentClass = [];
        //     let classID = "";
        //     let studentRoom = [];
        //     let roomID = "";

        //     result.forEach(element => {
        //     //    console.log(JSON.stringify(element));
        //     //    student.data.push(element);
        //        if (classID != element.CLASS_ID) {
        //            classID = element.CLASS_ID;
        //        } else{
        //             if (roomID != element.ROOM_ID) {
        //                 roomID = element.ROOM_ID;
        //              //    studentClass.push({
        //              //        "class":classID,
        //              //    });
        //              //     studentClass.rooms.push(element);
        //             } else{
        //                 studentRoom.push(element);

        //                 // studentClass.push({
        //                 //     "class": classID,
        //                 //     "rooms":studentRoom
        //                 // })
        //                 // console.log(studentClass);
        //                 // console.log(element);
        //             }
        //        }
        //     });

        //     // console.log(studentClass);
          
        //     res.status(200);
        //     res.send(JSON.stringify(studentRoom));
        // });
       
    });
    app.use((req, res, next) => {
        let err = new Error("ไม่พบ path ที่คุณต้องการ");
        err.status = 404;
        // console.log(req);
        next(err);
    });

    
});